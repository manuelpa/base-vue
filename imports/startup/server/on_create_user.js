import { Accounts } from 'meteor/accounts-base';

Accounts.config({ forbidClientAccountCreation: true });

Accounts.onCreateUser((options, user) => {
  const targetUser = { ...user };
  targetUser.profile = options.profile || {};
  return targetUser;
});
