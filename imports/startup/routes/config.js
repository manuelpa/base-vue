import Router from 'vue-router';

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'root',
      component: () => import('../../ui/layouts/General.vue'),
      redirect: '/home',
      children: [
        {
          path: '/signup',
          name: 'signup',
          component: () => import('../../ui/pages/Signup.vue'),
        },
        {
          path: '/login',
          name: 'login',
          component: () => import('../../ui/pages/Login.vue'),
        },
        {
          path: '/home',
          name: 'home',
          component: () => import('../../ui/pages/Home.vue'),
        },
      ],
    },
  ],
});

export default router;
